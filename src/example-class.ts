export class DanIsANobberMessager {
    
    private message: string

    constructor(){
        this.message = 'Dan is a nobber!'
    }

    displayMessageInConsole(){
        console.log(this.message)
    }

}